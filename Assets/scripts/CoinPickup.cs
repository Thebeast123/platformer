﻿using UnityEngine;
using System.Collections;

public class CoinPickup : MonoBehaviour {

	public int pointsToAdd;

	public AudioSource pickupSoundEffect;

	void OnTriggerEnter2D (Collider2D other)
	{
		if(other.GetComponent<PlayerController>() == null)
			return;

		ScoreManager.Addpoints(pointsToAdd);

		pickupSoundEffect.Play();

		Destroy (gameObject);
	}
}
