﻿using UnityEngine;
using System.Collections;
public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private float moveVelocity;
	public float jumpHeight;

	private Animator animator;
	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded;

	private bool doubleJumped;

	public Transform firePoint;
	public GameObject FireBall;

	public float shotDelay;
	private float shotDelayCounter;

	public float knockback;
	public float knockbackLength;
	public float knockbackCount;
	public bool knockFromRight;

	private Rigidbody2D myrigidbody2D;

	void Start(){
		myrigidbody2D = GetComponent<Rigidbody2D>();
		animator = GetComponent<Animator>();
	}

	void FixedUpdate()
	{
		grounded = Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, whatIsGround);
	}

	void Update(){

		if(grounded)
			doubleJumped = false;

		if(Input.GetKeyDown (KeyCode.Space) && grounded)
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(rigidbody2D.velocity.x, jumpHeight);
			Jump();
		}

		if(Input.GetKeyDown (KeyCode.Space) && !doubleJumped && !grounded)
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(rigidbody2D.velocity.x, jumpHeight);
			Jump();
			doubleJumped = true;
		}

		if(Input.GetKeyDown (KeyCode.UpArrow) && grounded)
		{
			//GetComponent<Rigidbody2D>().velocity = new Vector2(rigidbody2D.velocity.x, jumpHeight);
			Jump();
			doubleJumped = true;
		}

		if(Input.GetKeyDown (KeyCode.UpArrow) && !doubleJumped && !grounded)
		{
			//<Rigidbody2D>().velocity = new Vector2(rigidbody2D.velocity.x, jumpHeight);
			Jump();
		}

		moveVelocity = 0f;


		if(Input.GetKeyDown (KeyCode.RightArrow))
		{
			myrigidbody2D.velocity = new Vector2(moveSpeed,myrigidbody2D.velocity.y);
			//moveVelocity = moveSpeed;
			animator.SetTrigger("Player-Idle");
		}

		if(Input.GetKeyDown (KeyCode.LeftArrow))
		{
			myrigidbody2D.velocity = new Vector2(-moveSpeed,myrigidbody2D.velocity.y);
			//moveVelocity = -moveSpeed;
			animator.SetTrigger("Player-Idle");
		}

		if(knockbackCount <= 0)
		{
			//myrigidbody2D.velocity  = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);	
		}else{
			if(knockFromRight)
				myrigidbody2D.velocity = new Vector2(-knockback, knockback);
			if(!knockFromRight)
				myrigidbody2D.velocity = new Vector2(knockback, knockback);
			knockbackCount -= Time.deltaTime;
		}

		//myrigidbody2D.velocity = new Vector2(moveVelocity, GetComponent<Rigidbody2D>().velocity.y);	
		if(Input.GetKeyDown(KeyCode.X))
		{
			Instantiate(FireBall, firePoint.position, firePoint.rotation);
			shotDelayCounter = shotDelay;
		}

		if(Input.GetKeyDown (KeyCode.X))
		{
			shotDelayCounter -= Time.deltaTime;

			if(shotDelayCounter <= 0)
			{
				shotDelayCounter = shotDelay;
				Instantiate(FireBall, firePoint.position, firePoint.rotation);
			}

		}

	}

	public void Jump()
	{
		myrigidbody2D.velocity = new Vector2(myrigidbody2D.velocity.x, jumpHeight);
	}
}