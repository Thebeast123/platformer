﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HealthManager : MonoBehaviour {

	public int maxPlayerHealth;

	public static int playerhealth;

	Text text;

	public LevelManager levelManager;

	public bool isDead;

	private LifeManager lifeSystem;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();

		playerhealth = maxPlayerHealth;

		levelManager = FindObjectOfType<LevelManager>();

		lifeSystem = FindObjectOfType<LifeManager>();

		isDead = false;

	}
	
	// Update is called once per frame
	void Update () {
	  if(playerhealth <= 0 && !isDead)
		{
			playerhealth = 0;
			levelManager.RespawnPlayer();
			lifeSystem.TakeLife();
			isDead = true;
		}

		text.text = "" + playerhealth;
	}

	public static void HurtPlayer(int damageToGive)
	{
		playerhealth -= damageToGive;
	}

	public void FullHealth()
	{
		playerhealth = maxPlayerHealth;
	}
}